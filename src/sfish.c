#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <readline/readline.h>
#include <errno.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>


#include "sfish.h"


void helpCommand(){
    printf("help: Print a list of all builtin command \n");
    printf("exit: Exits the shell \n");
    printf("cd: change the current working directory \n");
    printf("pwd: prints the absolute path of the current working directory \n");
}

void pwdCommand(){
    char buf[80];
    getcwd(buf, sizeof(buf));
    printf("%s\n", buf);
}

void cdCommand(char* argv[], char* prevDirectory, char* currentDirectory, char* string){
    //char* argument1;
    char* argument2;
    char *home = getenv("HOME");
    int len = strlen(home);

    //argument1 = strtok(input," ");
    //if(argument1 != NULL){
        argument2 = strtok(NULL, " ");
        //go to the home directory
        if(argv[1] == NULL){
            prevDirectory = getcwd(prevDirectory, 1024);
            chdir(getenv("HOME"));
            strcpy(string, "~ :: yiyyuan ");

        }
        //change to the preDirectory
        else if(strcmp(argv[1], "-") == 0){
            if(*prevDirectory == 0){
                printf("bash: cd: OLDPWD not set\n");

            }
            else{
                currentDirectory = getcwd(currentDirectory, 1024);
                chdir(prevDirectory);
                strcpy(prevDirectory, currentDirectory);
                currentDirectory = getcwd(currentDirectory, 1024);
                if(strlen(currentDirectory) >= len){
                    char dest[1024] = {""};
                    char temp[1024] = {""};
                    strcpy(dest, "~");
                    strncpy(temp, currentDirectory+len, strlen(currentDirectory)-len);
                    strcat(dest, temp);
                    strcat(dest, " :: yiyyuan ");
                    strcpy(string, dest);

                }
                else{
                    strcpy(string, currentDirectory);
                    strcat(string," :: yiyyuan ");
                }

            }


        }
        //is . stay in current directory
        else if(strcmp(argv[1], ".") == 0){
            currentDirectory = getcwd(currentDirectory, 1024);
            if(strlen(currentDirectory) >= len){
                char dest[1024] = {""};
                char temp[1024] = {""};
                strcpy(dest, "~");
                strncpy(temp, currentDirectory+len, strlen(currentDirectory)-len);
                strcat(dest, temp);
                strcat(dest, " :: yiyyuan ");
                strcpy(string, dest);

            }
            else{
                strcpy(string, currentDirectory);
                strcat(string," :: yiyyuan ");
            }
        }
        //is .. go to previous working directory
        else if(strcmp(argv[1], "..") ==0 ){
            //printf("%s\n", prevDirectory);
            currentDirectory = getcwd(currentDirectory, 1024);
            chdir("..");
            strcpy(prevDirectory, currentDirectory);
            currentDirectory = getcwd(currentDirectory, 1024);
            if(strlen(currentDirectory) >= len){
                char dest[1024] = {""};
                char temp[1024] = {""};
                strcpy(dest, "~");
                strncpy(temp, currentDirectory+len, strlen(currentDirectory)-len);
                strcat(dest, temp);
                strcat(dest, " :: yiyyuan ");
                strcpy(string, dest);

            }
            else{
                strcpy(string, currentDirectory);
                strcat(string," :: yiyyuan ");
            }

        }
        //change to certain directory
        else {
            char buf[1024];
            getcwd(buf, sizeof buf);
            //prevDirectory = getcwd(prevDirectory,1024);
            currentDirectory = strcat(currentDirectory,"/");
            currentDirectory = strcat(currentDirectory,argument2);
            int check = chdir(currentDirectory);
            if( check == -1){
                printf("sfish: EXEC_NOT_FOUND: command not found\n");
                currentDirectory = getcwd(currentDirectory, 1024);
            }
            else{
                strcpy(prevDirectory,buf);
                if(strlen(currentDirectory) >= len){
                    char dest[1024] = {""};
                    char temp[1024] = {""};
                    strcpy(dest, "~");
                    strncpy(temp, currentDirectory+len, strlen(currentDirectory)-len);
                    strcat(dest, temp);
                    strcat(dest, " :: yiyyuan ");
                    strcpy(string, dest);

                }
                else{
                    strcpy(string, currentDirectory);
                    strcat(string," :: yiyyuan ");
                }

            }

        }

}




volatile sig_atomic_t pid;

void sigchld_handler(int s){
    int olderrno = errno;
    pid = waitpid(-1, NULL, 0);
    errno = olderrno;
}


void sigint_handler(int s){
    //printf("\n");
    //exit(0);
}

// void sigtstp_handler(int s){

// }




void exe(char **argv,int argc,int indicator){

    sigset_t mask, prev;
    signal(SIGCHLD, sigchld_handler);
    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);

    //while(1){
        sigprocmask(SIG_BLOCK, &mask, &prev);
        if((pid=fork()) == 0){
            signal(SIGINT, SIG_DFL);
            //signal(SIGTSTP,SIG_DFL);

            sigprocmask(SIG_SETMASK, &prev, NULL);

            // do redierction >
            if(indicator == 1){
                int i =0;
                int position ;
                for(;i<argc;i++){
                    if(*argv[i] == '>'){
                        position = i+1;
                    }
                    //i++;
                }
                char* filename = argv[position];
                //printf("ssssss%s\n",filename);
                int fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IXUSR);

                dup2(fd,STDOUT_FILENO);
                position = position -1;
                for(;position<argc;position++){
                    argv[position] = NULL;
                }
            }
            //do redirection <
            else if(indicator == 2 ){
                int i =0;
                int position = 0;
                for(;i<argc;i++){
                    if(*argv[i] == '<'){
                        position = i+1;
                    }
                }
                char* filename = argv[position];
                //printf("ssssss%s\n",filename);
                int fd = open(filename, O_RDONLY , S_IRUSR | S_IWUSR | S_IXUSR);

                dup2(fd,STDIN_FILENO);
                position = position -1;
                for(;position<argc;position++){
                    argv[position] = NULL;
                }
            }
            else if(indicator == 3){

                int i =0;
                int position1 ;
                for(;i<argc;i++){
                    if(*argv[i] == '<'){
                        position1 = i+1;
                    }
                    //i++;
                }
                char* infile = argv[position1];
                int fd1 = open(infile, O_RDONLY , S_IRUSR | S_IWUSR | S_IXUSR);
                dup2(fd1,STDIN_FILENO);

                i =0;
                int position2 ;
                for(;i<argc;i++){
                    if(*argv[i] == '>'){
                        position2 = i+1;
                    }
                    //i++;
                }

                char* outfile = argv[position2];


                // > later
                int fd2 = open(outfile, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IXUSR);
                dup2(fd2,STDOUT_FILENO);

                position1 = position1 -1;
                for(;position1<argc;position1++){
                    argv[position1] = NULL;
                }


            }

            else if(indicator == 4){
                int i =0;
                int position1 ;
                for(;i<argc;i++){
                    if(*argv[i] == '<'){
                        position1 = i+1;
                    }
                    //i++;
                }
                char* infile = argv[position1];

                int fd1 = open(infile, O_RDONLY , S_IRUSR | S_IWUSR | S_IXUSR);
                dup2(fd1,STDIN_FILENO);


                i =0;
                int position2 ;
                for(;i<argc;i++){
                    if(*argv[i] == '>'){
                        position2 = i+1;
                    }
                    //i++;
                }
                position2 = position2 -1;
                for(;position2<argc;position2++){
                    argv[position2] = NULL;
                }

            }
            else if(indicator == 5){

            }

            int check;
            check = execvp(argv[0], argv);
            if(check == -1){
                if(errno == 2){
                    printf(EXEC_NOT_FOUND, *argv);

                }
                else{
                    printf(EXEC_ERROR,strerror(errno));
                }
            }
            sigprocmask(SIG_SETMASK, &prev, NULL);
            exit(0);
        }


        pid = 0;
        while(!pid){
            sigsuspend(&prev);
        }
        sigprocmask(SIG_SETMASK, &prev, NULL);

    //}
    //exit(0);
}



void changeCommand(char* input){
    char buf[1024];
    strcpy(buf,input);
    int i = 0;
    int m = 0;
    int j = strlen(input);
    while(m<j){
        if(*(buf+m) == '>'){
            *(input+i) = ' ';
            i++;
            *(input+i) = '>';
            i++;
            *(input+i) = ' ';

        }
        else if(*(buf+m) == '<'){
            *(input+i) = ' ';
            i++;
            *(input+i) = '<';
            i++;
            *(input+i) = ' ';
        }
        else if(*(buf+m) == '|'){
            *(input+i) = ' ';
            i++;
            *(input+i) = '|';
            i++;
            *(input+i) = ' ';
        }
        else{
            *(input+i) = buf[m];
        }
        i++;
        m++;
    }
    *(input+i) = '\0';
    //printf("%s",input);

}

int charCount(char* input, char a){
    int i = 0;
    int count = 0;
    int len = strlen(input);
    while(i<len){
        if(*(input+i) == a){
            count++;
        }
        i++;
    }
    return count;
}


void exePipe(char **argv,int argc){

    sigset_t mask, prev;
    signal(SIGCHLD, sigchld_handler);
    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);
    int i = 0;
    int temp = 0;
    int tempFd = 0;


    while(argv[i] != NULL){
        if(*argv[i] == '|' ){
            int pipe_fd[2];
            char* argvTrue[i-temp];
            int j = 0;
            for(; j<(i-temp); j++){
                argvTrue[j] = argv[temp+j];
            }
            argvTrue[j] = '\0';
            temp = i+1;

            pipe(pipe_fd);
            sigprocmask(SIG_BLOCK, &mask, &prev);

            if((pid=fork()) == 0){

                sigprocmask(SIG_SETMASK, &prev, NULL);
                signal(SIGINT, SIG_DFL);
                //signal(SIGTSTP,SIG_DFL);
                dup2(tempFd, STDIN_FILENO);

                if(argv[i+1] != NULL){
                    dup2(pipe_fd[1],STDOUT_FILENO);
                }
                close(pipe_fd[0]);
                int check;
                check = execvp(argvTrue[0], argvTrue);
                if(check == -1){
                    if(errno == 2){
                        printf(EXEC_NOT_FOUND, *argv);

                    }
                    else{
                        printf(EXEC_ERROR,strerror(errno));
                    }
                }
                sigprocmask(SIG_SETMASK, &prev, NULL);
                exit(0);
            }
            pid = 0;
            //printf("parent\n");
            while(!pid){
                sigsuspend(&prev);
            }
            close(pipe_fd[1]);
            tempFd = pipe_fd[0];
            sigprocmask(SIG_SETMASK, &prev, NULL);
        }
        i++;
    }

}


int pwdExe(char** argvNew, char** argv, int argc){
    char buf[80];
    getcwd(buf, sizeof(buf));

    argvNew[0]= "echo";
    argvNew[1] = buf;
    argc = argc +1;

    int i = 2;
    for(;i<argc;i++){
        argvNew[i] = argv[i-1];
    }
    argvNew[argc] = '\0';
    return argc;
}



int helpExe(char** argvNew, char** argv, int argc){
    char* buf ="help: Print a list of all builtin command\nexit: Exits the shell\ncd: change the current working directory\npwd: prints the absolute path of the current working directory";
    argvNew[0]= "echo";
    argvNew[1] = buf;
    argc = argc +1;

    int i = 2;
    for(;i<argc;i++){
        argvNew[i] = argv[i-1];
    }
    argvNew[argc] = '\0';
    return argc;
}




void jobCommand(){

}