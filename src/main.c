#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <readline/readline.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>


#include "sfish.h"
#include "debug.h"

#define NRM "\033[0m"
#define RED "\033[1;31m"
#define GRN "\033[1;32m"
#define YEL "\033[1;33m"
#define BLU "\033[1;34m"
#define MAG "\033[1;35m"
#define CYN "\033[1;36m"
#define WHT "\033[1;37m"
#define BWN "\033[0;33m"


void helpCommand();
void pwdCommand();
void cdCommand(char* argv[], char* prevDirectory, char* currentDirectory, char* string);
void exe(char** argv,int argc, int indicator);
int checkFileStat(char* tempPath);
//void sigint_handler(int s);
void changeCommand(char* input);
int charCount(char* input, char a);
//void sigtstp_handler(int s);
void jobCommand();
void exePipe(char **argv,int argc);
void exeHelp(char** argv, int argc);
int pwdExe(char** argvNew, char** argv, int argc);
int helpExe(char** argvNew, char** argv, int argc);




void colorCommand(char* color, char** argv){
    if(strcmp(argv[1],"RED") == 0){
        strcpy(color,RED);

    }
    else if(strcmp(argv[1],"GRN") == 0){
        strcpy(color,GRN);
    }
    else if(strcmp(argv[1],"YEL") == 0){
        strcpy(color,YEL);
    }
    else if(strcmp(argv[1],"BLU") == 0){
        strcpy(color,BLU);
    }
    else if(strcmp(argv[1],"MAG") == 0){
        strcpy(color,MAG);
    }
    else if(strcmp(argv[1],"CYN") == 0){
        strcpy(color, CYN);
    }
    else if(strcmp(argv[1],"WHT") == 0){
        strcpy(color,WHT);
    }
    else if(strcmp(argv[1],"BWN") == 0){
        strcpy(color, BWN);
    }

}



int main(int argc, char *argv[], char* envp[]) {



    //signal(SIGTSTP,sigtstp_handler);

    char* prevDirectory;
    char* currentDirectory;
    char* input;
    char* string;
    bool exited = false;
    char* color;

    prevDirectory = malloc(1024);
    currentDirectory = malloc(1024);
    string = malloc(1024);
    color = malloc(1024);

    memset(prevDirectory, 0, 1024);
    memset(currentDirectory, 0, 1024);
    memset(string, 0, 1024);
    memset(string, 0, 1024);

    currentDirectory = getcwd(currentDirectory, 1024);
    color = strcpy(color, NRM);
    //char *path = getenv("PATH");


    strcpy(string, "~/yiyyuan/hw4 :: yiyyuan ");
    signal(SIGINT,SIG_IGN);

    if(!isatty(STDIN_FILENO)) {
        // If your shell is reading from a piped file
        // Don't have readline write anything to that file.
        // Such as the prompt or "user input"
        if((rl_outstream = fopen("/dev/null", "w")) == NULL){
            perror("Failed trying to open DEVNULL");
            exit(EXIT_FAILURE);
        }
    }

    do {
        //string = KNRM;
        // printf("%s",color);
         printf("taojingqiu shi dashibbi");
        // // printf("%s%s", KRED,">> " );
        // input = readline(">> " NRM);
         int indicator = 0;
        input = readline(">>");

        // write(1, "\e[s", strlen("\e[s"));
        // write(1, "\e[20;10H", strlen("\e[20;10H"));
        // write(1, "SomeText", strlen("SomeText"));
        // write(1, "\e[u", strlen("\e[u"));

        //if(signal(SIGINT, sigint_handler) == SIG_ERR){
            //unix_error("signal error");
            //continue;
        //}


        // If EOF is read (aka ^D) readline returns NULL
        //printf("%s\n",input);


        if(input == NULL || strcmp(input, "")== 0) {
            continue;
        }


        else{
            //printf("%s",input);
            int c1;
            int c2;
            int c3;
            char* tempInput = input;

            char* delim;    //point to the first space
            char* argv[strlen(tempInput)];
            int argc = 0;

            char temp1[1024] ;
            strcpy(temp1,input);
            char* argument1;
            argument1 = strtok(temp1," ");

            changeCommand(tempInput);

            c1 = charCount(tempInput,'>');
            c2 = charCount(tempInput,'<');
            c3 = charCount(tempInput,'|');


            tempInput[strlen(tempInput)] = ' ';
            while(*tempInput && (*tempInput == ' ')){
                //igore leading space
                tempInput++;

            }
            //build the argv list
            while((delim = strchr(tempInput, ' '))){
                argv[argc++] = tempInput;
                *delim = '\0';
                tempInput = delim + 1;
                while(*tempInput && (*tempInput == ' '))     //ignore space
                    tempInput++;
            }

            if(c3 == 0){
                argv[argc] = NULL;

            }
            else{
                argv[argc] = "|";
                argv[argc+1] = NULL;
                argc++;
            }


            if(argument1 == NULL){
                continue;
            }
            else{
                //part 1 and part 2
                if(c1 ==0 && c2 == 0 && c3 ==0){
                    //help
                    if(strcmp(argument1, "help") == 0){
                        //printf("");
                         helpCommand();
                    }
                    //exit
                    else if(strcmp(argument1,"exit") == 0){
                        exited = 1;
                        //exit(3);
                    }
                    //cd
                    else if(strcmp(argument1, "cd") == 0){
                        //printf("%s\n",input);
                        cdCommand(argv, prevDirectory, currentDirectory, string);
                    }
                    //pwd
                    else if(strcmp(argument1, "pwd") == 0){
                        pwdCommand();
                    }
                    else if(strcmp(argument1, "color") == 0){
                        colorCommand(color,argv);

                    }
                    else if(strcmp(argument1, "jobs") == 0){
                        jobCommand();

                    }
                    else if(strcmp(argument1, "fg") == 0){

                    }
                    else if(strcmp(argument1,"kill") == 0){

                    }
                    else {
                        indicator =0 ;
                        exe(argv,argc,indicator);
                    }

                }
                //part 3
                else{
                    int fail = 0;
                    for(int i= 0; i<argc; i++){
                        if((strcmp(argv[i],"<")==0)||(strcmp(argv[i],">")==0)){
                            if(argv[i+1] == NULL){
                                fail = 1;
                            }
                        }
                        if(strcmp(argv[i],"|")==0){
                            if(argv[i+1]!=NULL){
                                if(strcmp(argv[i+1],"|") == 0){
                                    fail = 1;
                                }
                            }

                        }
                    }
                    if((strcmp(argv[0],"<")==0)||(strcmp(argv[0],">")==0)||(strcmp(argv[0],"|") == 0)){
                        fail = 2;
                    }

                    if(fail != 0){
                        if(fail == 1){
                            printf(SYNTAX_ERROR,"command not found");
                        }
                        else{
                            printf(SYNTAX_ERROR, "unexpected token `newline'");
                        }
                    }
                    else{
                    //prog1 [ARGS] < input.txt
                        if(c1 ==0 && c2 == 1 && c3 == 0){

                            indicator = 2;
                            if(strcmp(argv[0], "pwd") == 0){
                                char* argvNew[argc+1];
                                argc = pwdExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);
                            }
                            else if(strcmp(argv[0],"help") == 0){
                                char* argvNew[argc+1];
                                argc = helpExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);

                            }
                            else{
                                exe(argv, argc, indicator);
                            }

                        }
                        //prog1 [ARGS] > output.txt
                        else if(c1 == 1 && c2 ==0 && c3 == 0){
                            indicator = 1;
                            //open the output.txt
                            if(strcmp(argv[0], "pwd") == 0){
                                char* argvNew[argc+1];
                                argc = pwdExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);

                            }
                            else if(strcmp(argv[0],"help") == 0){
                                char* argvNew[argc+1];
                                argc = helpExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);

                            }

                            else{
                                exe(argv, argc, indicator);

                            }

                        }

                        //prog1 [ARGS] < input.txt > output.txt
                        else if(c1 ==1 && c2 ==1 && c3 == 0){
                            int poBig = 0;
                            int poSmalle = 0;
                            for(int i=0;i<argc;i++){
                                if(*argv[i] == '>'){
                                    poBig = i;
                                }
                                if(*argv[i] == '<'){
                                    poSmalle = i;
                                }

                            }
                            if(poBig > poSmalle){
                                indicator = 3;
                            }
                            else{
                                indicator = 4;
                            }
                            if(strcmp(argv[0], "pwd") == 0){
                                char* argvNew[argc+1];
                                argc = pwdExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);

                            }
                            else if(strcmp(argv[0],"help") == 0){
                                char* argvNew[argc+1];
                                argc = helpExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);

                            }
                            else{
                                exe(argv,argc,indicator);
                            }
                        }
                        //prog1 [ARGS] | prog2 [ARGS] | .....
                        else if(c1 == 0 && c2 == 0 && c3!=0){
                            indicator = 5;
                            if(strcmp(argv[0], "pwd") == 0){
                                char* argvNew[argc+1];
                                argc = pwdExe(argvNew,argv,argc);
                                exePipe(argvNew,argc);

                            }
                            else if(strcmp(argv[0],"help") == 0){
                                char* argvNew[argc+1];
                                argc = helpExe(argvNew,argv,argc);
                                exe(argvNew,argc,indicator);

                            }
                            else{
                                exePipe(argv,argc);
                            }

                        }
                        else if(c1 == 1 && c2 ==0 && c3!=0){
                            indicator = 6;

                        }
                        else if(c1 == 0 && c2 ==1 && c3!=0){
                            indicator = 7;
                        }
                        else{
                            printf(SYNTAX_ERROR, "the format is wrong");
                        }

                    }

                }
            }

        }
        // Currently nothing is implemented
        //printf(EXEC_NOT_FOUND, input);

        // You should change exit to a "builtin" for your hw.
        //exited = strcmp(input, "exit") == 0;

        // Readline mallocs the space for input. You must free it.
        rl_free(input);


    } while(!exited);
    free(prevDirectory);
    free(currentDirectory);
    free(color);
    free(string);


    debug("%s", "user entered 'exit'");
    exit(0);

    return EXIT_SUCCESS;
}
